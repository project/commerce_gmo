<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_gmo\Unit;

use Drupal\commerce_gmo\GmoJsonClient;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @coversDefaultClass \Drupal\commerce_gmo\GmoJsonClient
 *
 * @group commerce_gmo
 */
class GmoJsonClientTest extends UnitTestCase {

  /**
   * We just test this one method, so we can test ::doCall.
   *
   * @covers ::searchTrade
   * @covers ::doCall
   */
  public function testSearchTrade() {
    $response_data = [
      'foo' => 'bar',
      'bar' => 'baz',
    ];
    $responses = [
      new Response(200, [], json_encode($response_data)),
    ];

    $gmo_client = GmoJsonClient::createClient($this->getMockHttpClient($responses), 'foo', 'bar', TRUE);
    $result = $gmo_client->searchTrade('1-1');
    $this->assertEquals($response_data, $result);
  }

  /**
   * We just test this one method, so we can test ::doCall.
   *
   * @covers ::searchTrade
   * @covers ::doCall
   */
  public function testSearchTradeException() {
    $responses = [
      new Response(400, [], json_encode([
        [
          'errCode' => 'E01',
          'errInfo' => 'E00001',
        ],
      ])),
    ];

    $gmo_client = GmoJsonClient::createClient($this->getMockHttpClient($responses), 'foo', 'bar', TRUE);

    $this->expectException(\Exception::class);
    $this->expectErrorMessage('Api returned errCode E01, errInfo E00001');
    $gmo_client->searchTrade('1-1');
  }

  /**
   * Get the mock http client.
   *
   * @param \GuzzleHttp\Psr7\Response[] $responses
   *   Array of Response object.
   *
   * @return \GuzzleHttp\Client
   *   The http client.
   */
  protected function getMockHttpClient(array $responses): Client {
    $mock = new MockHandler($responses);
    $handler_stack = HandlerStack::create($mock);
    return new Client(['handler' => $handler_stack]);
  }

}
