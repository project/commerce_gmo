<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_gmo\Kernel;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\Core\Form\FormState;
use Drupal\Core\Routing\CurrentRouteMatch;
use GuzzleHttp\Psr7\Response;

/**
 * @coversDefaultClass \Drupal\commerce_gmo\Plugin\Commerce\CheckoutPane\GmoReview
 *
 * @group commerce_gmo
 */
class GmoReviewPaneTest extends GmoKernelTestBase {

  /**
   * The GMO review pane.
   *
   * @var \Drupal\commerce_gmo\Plugin\Commerce\CheckoutPane\GmoReview
   */
  protected $pane;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_checkout',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $payment_method = PaymentMethod::create([
      'payment_gateway' => $this->paymentGateway->id(),
      'type' => 'credit_card',
      'remote_id' => 'gmotokenthisis',
    ]);
    $payment_method->save();
    $this->order->set('payment_method', $payment_method)->save();

    $route_match = $this->prophesize(CurrentRouteMatch::class);
    $route_match
      ->getParameter('commerce_order')
      ->willReturn($this->order);
    $this->container->set('current_route_match', $route_match->reveal());

    $checkout_flow_manager = $this->container->get('plugin.manager.commerce_checkout_flow');
    $checkout_flow = $checkout_flow_manager->createInstance('multistep_default', []);

    $checkout_pane_manager = $this->container->get('plugin.manager.commerce_checkout_pane');
    $this->pane = $checkout_pane_manager->createInstance('gmo_review', [], $checkout_flow);
  }

  /**
   * @covers ::buildPaneForm
   * @covers ::is3dsRedirect
   */
  public function testReviewPanePrepare3ds0() {
    $form_state = new FormState();
    $form_state->setUserInput([]);
    $complete_form = [];

    $register_response_data = $this->getRegisterTransactionResponseData();
    $responses = [
      new Response(200, [], json_encode($register_response_data)),
      new Response(200, [], json_encode(['acs' => '0'])),
    ];
    $this->populateHttpClient($responses);

    $this->expectException(NeedsRedirectException::class);
    $this->pane->buildPaneForm([], $form_state, $complete_form);
  }

  /**
   * @covers ::buildPaneForm
   * @covers ::is3dsRedirect
   */
  public function testReviewPanePrepare3ds1() {
    $form_state = new FormState();
    $form_state->setUserInput([]);
    $complete_form = [];

    $register_response_data = $this->getRegisterTransactionResponseData();
    $execution_response_data = $this->getExecuteTransactionResponseData(1);
    $responses = [
      new Response(200, [], json_encode($register_response_data)),
      new Response(200, [], json_encode($execution_response_data)),
    ];
    $this->populateHttpClient($responses);

    $build = $this->pane->buildPaneForm([], $form_state, $complete_form);
    $this->assertEquals('/commerce-gmo/acs/modal/' . $this->order->id(), $build['3ds_link']['#url']->toString());
    $this->assertEquals('use-ajax', $build['3ds_link']['#attributes']['class'][0]);
    $this->assertEquals('modal', $build['3ds_link']['#attributes']['data-dialog-type']);

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($this->order->id());
    $this->assertEquals($register_response_data['accessID'], $order->getData('gmo_access_id'));
    $this->assertEquals($register_response_data['accessPass'], $order->getData('gmo_access_pass'));
    $acs_data = [
      'PaReq' => $execution_response_data['paReq'],
      'MD' => $execution_response_data['md'],
      'ACSUrl' => $execution_response_data['acsurl'],
    ];
    $this->assertEquals($acs_data, $order->getData('gmo_acs_data'));
  }

  /**
   * @covers ::buildPaneForm
   * @covers ::is3dsRedirect
   */
  public function testReviewPanePrepare3ds2() {
    $form_state = new FormState();
    $form_state->setUserInput([]);
    $complete_form = [];

    $register_response_data = $this->getRegisterTransactionResponseData();
    $execution_response_data = $this->getExecuteTransactionResponseData(2);
    $responses = [
      new Response(200, [], json_encode($register_response_data)),
      new Response(200, [], json_encode($execution_response_data)),
    ];
    $this->populateHttpClient($responses);

    $build = $this->pane->buildPaneForm([], $form_state, $complete_form);
    $this->assertEquals($execution_response_data['redirectUrl'], $build['3ds_link']['#url']->toString());

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($this->order->id());
    $this->assertEquals($register_response_data['accessID'], $order->getData('gmo_access_id'));
    $this->assertEquals($register_response_data['accessPass'], $order->getData('gmo_access_pass'));
  }

  /**
   * @covers ::buildPaneForm
   * @covers ::is3dsRedirect
   * @covers ::handle3dsRedirect
   */
  public function testIs3dsRedirect1() {
    $this->order->setData('gmo_access_id', 'gmoaccessid');
    $this->order->save();
    $this->pane->setOrder($this->order);
    $form_state = new FormState();
    // Fist set a different access id.
    $user_input = [
      'MD' => 'gmoaccessid_incorrect',
      'PaRes' => 'gmopares',
    ];
    $form_state->setUserInput($user_input);
    $complete_form = [];
    try {
      $this->pane->buildPaneForm([], $form_state, $complete_form);
      $this->fail('Exception was not thrown');
    }
    catch (\Exception $e) {
      $payment_methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadMultiple();
      $this->assertCount(0, $payment_methods);
      $this->assertTrue($e instanceof NeedsRedirectException);
      $this->assertEquals('/checkout/1/order_information', $e->getResponse()->getTargetUrl());
    }

    // Now use the correct one.
    $user_input = [
      'MD' => 'gmoaccessid',
      'PaRes' => 'gmopares',
    ];
    $this->order->setData('gmo_access_id', 'gmoaccessid');
    $this->order->save();
    $this->pane->setOrder($this->order);
    $form_state->setUserInput($user_input);
    $complete_form = [];
    $build = $this->pane->buildPaneForm([], $form_state, $complete_form);

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($this->order->id());
    $this->assertEquals($user_input['PaRes'], $order->getData('gmo_pa_res'));
    $this->assertArrayNotHasKey('3ds_link', $build);
  }

  /**
   * @covers ::buildPaneForm
   * @covers ::is3dsRedirect
   * @covers ::handle3dsRedirect
   */
  public function testIs3dsRedirect2() {
    $this->order->setData('gmo_access_id', 'gmoaccessid');
    $this->order->save();
    $this->pane->setOrder($this->order);
    $form_state = new FormState();
    // Fist set a different access id.
    $user_input = [
      'AccessID' => 'gmoaccessid_incorrect',
    ];
    $form_state->setUserInput($user_input);
    $complete_form = [];
    try {
      $this->pane->buildPaneForm([], $form_state, $complete_form);
      $this->fail('Exception was not thrown');
    }
    catch (\Exception $e) {
      $payment_methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadMultiple();
      $this->assertCount(0, $payment_methods);
      $this->assertTrue($e instanceof NeedsRedirectException);
      $this->assertEquals('/checkout/1/order_information', $e->getResponse()->getTargetUrl());
    }

    // Now use the correct one.
    $user_input = [
      'AccessID' => 'gmoaccessid',
    ];
    $this->order->setData('gmo_access_id', 'gmoaccessid');
    $this->order->save();
    $this->pane->setOrder($this->order);
    $form_state->setUserInput($user_input);
    $complete_form = [];
    $build = $this->pane->buildPaneForm([], $form_state, $complete_form);
    $this->assertArrayNotHasKey('3ds_link', $build);
  }

  /**
   * @covers ::buildPaneForm
   * @covers ::is3dsRedirect
   */
  public function testStepSubmission() {
    $this->order->setData('gmo_access_id', 'gmoaccessid')->save();
    $form_state = new FormState();
    // Fist set a different access id.
    $user_input = [
      'some_drupal_form_data' => 'foobar',
    ];
    $form_state->setUserInput($user_input);
    $complete_form = [];
    $build = $this->pane->buildPaneForm([], $form_state, $complete_form);
    $this->assertArrayNotHasKey('3ds_link', $build);
  }

  /**
   * @covers ::buildPaneForm
   * @covers ::is3dsRedirect
   */
  public function testExceptionHandling() {
    $this->order->setData('gmo_access_id', 'gmoaccessid')->save();
    $responses = [
      new Response(403, [], json_encode([
        [
          'errCode' => 'E01',
          'errInfo' => 'E00001',
        ],
      ])),
    ];
    $this->populateHttpClient($responses);

    $complete_form = [];
    $form_state = new FormState();
    $form_state->setUserInput([]);

    $this->expectException(NeedsRedirectException::class);
    $this->pane->buildPaneForm([], $form_state, $complete_form);
  }

  /**
   * Get register transaction response data.
   *
   * @return array
   *   The data.
   */
  protected function getRegisterTransactionResponseData(): array {
    $response_data = [
      'accessID' => 'foobar',
      'accessPass' => 'barbaz',
    ];

    return $response_data;
  }

  /**
   * Get register transaction response data.
   *
   * @param int $acs_version
   *   The acs version to return.
   *
   * @return array
   *   The data.
   */
  protected function getExecuteTransactionResponseData(int $acs_version): array {
    switch ($acs_version) {
      case 1;
        $response_data = [
          'acs' => (string) $acs_version,
          'md' => 'foobar',
          'paReq' => 'asdf123asdf',
          'acsurl' => 'https://www.example.com/acsurl',
        ];
        break;

      case 2;
        $response_data = [
          'acs' => (string) $acs_version,
          'redirectUrl' => 'https://www.example.com/redirecturl',
        ];
        break;
    }

    return $response_data;
  }

}
