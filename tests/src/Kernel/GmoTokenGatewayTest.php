<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_gmo\Kernel;

use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use GuzzleHttp\Psr7\Response;

/**
 * @coversDefaultClass \Drupal\commerce_gmo\Plugin\Commerce\PaymentGateway\GmoToken
 *
 * @group commerce_gmo
 */
class GmoTokenGatewayTest extends GmoKernelTestBase {

  /**
   * The payment entity.
   *
   * @var \Drupal\commerce_payment\Entity\Payment
   */
  protected $payment;

  /**
   * @covers ::createPaymentMethod
   */
  public function testCreatePaymentMethod() {
    $gateway_plugin = $this->getGmoTokenGateway();
    $payment_method = PaymentMethod::create([
      'payment_gateway' => $this->paymentGateway->id(),
      'type' => 'credit_card',
    ]);

    $payment_details = [
      'number' => '1234',
      'type' => 'Visa',
    ];
    try {
      $gateway_plugin->createPaymentMethod($payment_method, $payment_details);
      $this->fail('Exception was not thrown');
    }
    catch (\Exception $e) {
      $this->assertTrue($e instanceof InvalidRequestException);
      $this->assertEquals('$payment_details must contain the gmo_token key.', $e->getMessage());
    }

    $payment_details['gmo_token'] = 'foobar';
    $gateway_plugin->createPaymentMethod($payment_method, $payment_details);

    $payment_methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadMultiple();
    $this->assertCount(1, $payment_methods);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = reset($payment_methods);

    $this->assertEquals($payment_details['number'], $payment_method->get('card_number')->value);
    $this->assertEquals($payment_details['type'], $payment_method->get('card_type')->value);
    $this->assertEquals($payment_details['gmo_token'], $payment_method->getRemoteId());
    $this->assertFalse($payment_method->isReusable());
    $this->assertEquals($this->time->getRequestTime() + (60 * 30) - 5, $payment_method->getExpiresTime());
  }

  /**
   * @covers ::createPayment
   */
  public function testCreatePaymentAsserts() {
    $this->prepareForCreatPaymentTest();
    $payment_method = $this->payment->getPaymentMethod();
    $payment_method->setExpiresTime($this->time->getRequestTime() - 60);
    $gateway_plugin = $this->getGmoTokenGateway();

    $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
    try {
      $gateway_plugin->createPayment($this->payment);
      $this->fail('Exception was not thrown');
    }
    catch (\Exception $e) {
      $this->assertTrue($e instanceof DeclineException);
      $this->assertEquals('The provided payment method has expired', $e->getMessage());
      $this->assertCount(0, $payment_method_storage->loadMultiple());
    }
  }

  /**
   * @covers ::createPayment
   */
  public function testCreatePayment3ds1() {
    $this->order->setData('gmo_pa_res', 'foobar')->save();
    $this->prepareForCreatPaymentTest();

    $response_data = $this->getSecureTransactionResponseData();
    $responses = [
      new Response(200, [], json_encode($response_data)),
    ];
    $this->populateHttpClient($responses);

    $gateway = $this->getGmoTokenGateway();
    $gateway->createPayment($this->payment);

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $payment_storage->loadMultiple();
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);
    $this->assertEquals($this->order->getData('gmo_access_id'), $payment->get('access_id')->value);
    $this->assertEquals($this->order->getData('gmo_access_pass'), $payment->get('access_pass')->value);
    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals($response_data['tranID'], $payment->getRemoteId());
    $this->assertEquals($response_data['tranDate'], date('YmdHis', (int) $payment->getAuthorizedTime()));

    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $order = $order_storage->load($this->order->id());
    $this->assertEquals('1.0', $order->getData('gmo_3ds_version'));
    $this->assertNull($order->getData('gmo_access_id'));
    $this->assertNull($order->getData('access_pass'));
    $this->assertNull($order->getData('gmo_pa_res'));
    $this->assertNull($order->getData('gmo_acs_data'));
  }

  /**
   * @covers ::createPayment
   */
  public function testCreatePayment3ds2() {
    $this->prepareForCreatPaymentTest();

    $response_data = $this->getSecureTransactionResponseData();
    $responses = [
      new Response(200, [], json_encode($response_data)),
    ];
    $this->populateHttpClient($responses);

    $gateway = $this->getGmoTokenGateway();
    $gateway->createPayment($this->payment);

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $payment_storage->loadMultiple();
    $this->assertCount(1, $payments);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);
    $this->assertEquals($this->order->getData('gmo_access_id'), $payment->get('access_id')->value);
    $this->assertEquals($this->order->getData('gmo_access_pass'), $payment->get('access_pass')->value);
    $this->assertEquals('completed', $payment->getState()->value);
    $this->assertEquals($response_data['tranID'], $payment->getRemoteId());
    $this->assertEquals($response_data['tranDate'], date('YmdHis', (int) $payment->getAuthorizedTime()));

    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $order = $order_storage->load($this->order->id());
    $this->assertEquals('2.0', $order->getData('gmo_3ds_version'));
    $this->assertNull($order->getData('gmo_access_id'));
    $this->assertNull($order->getData('access_pass'));
    $this->assertNull($order->getData('gmo_pa_res'));
    $this->assertNull($order->getData('gmo_acs_data'));
  }

  /**
   * Get secure transaction response data.
   *
   * @return array
   *   The data.
   */
  protected function getSecureTransactionResponseData(): array {
    $gateway_plugin = $this->getGmoTokenGateway();
    $response_data = [
      'orderID' => $gateway_plugin->getOrderIdentifier($this->order),
      'forward' => 'abc1234',
      'method' => '1',
      'payTimes' => '1',
      'approve' => '1234567',
      'tranID' => '1234567890',
      'tranDate' => date('YmdHis', $this->time->getRequestTime()),
    ];

    $shop_pass = $gateway_plugin->getConfiguration()['shop_pass'];
    $response_data['checkString'] = md5(implode('', array_merge($response_data, [$shop_pass])));

    return $response_data;
  }

  /**
   * Prepares for testing the createPayment method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function prepareForCreatPaymentTest() {
    $payment_method = PaymentMethod::create([
      'payment_gateway' => $this->paymentGateway->id(),
      'type' => 'credit_card',
    ]);
    $payment_method->setExpiresTime($this->time->getRequestTime() + 60 * 60 * 24);

    $payment = Payment::create([
      'type' => 'gmo_payment',
      'payment_gateway' => $this->paymentGateway->id(),
      'remote_id' => 'foobar',
      'order_id' => $this->order->id(),
      'state' => 'new',
      'amount' => $this->order->getTotalPrice(),
    ]);

    $this->order->setData('gmo_access_id', 'foo');
    $this->order->setData('gmo_access_pass', 'bar');
    $this->order->save();

    $payment->set('payment_method', $payment_method)->save();
    $this->payment = $payment;
  }

}
