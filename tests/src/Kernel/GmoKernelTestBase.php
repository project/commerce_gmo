<?php

declare(strict_types = 1);

namespace Drupal\Tests\commerce_gmo\Kernel;

use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;

/**
 * Base class for Gmo kernel tests.
 */
abstract class GmoKernelTestBase extends CommerceKernelTestBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The payment gateway config entity.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $paymentGateway;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'entity_reference_revisions',
    'state_machine',
    'profile',
    'commerce_number_pattern',
    'commerce_order',
    'commerce_payment',
    'commerce_gmo',
  ];

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\Time|\Drupal\update_test\Datetime\TestTime|object|null
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->time = $this->container->get('datetime.time');

    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_payment');
    $this->installEntitySchema('commerce_payment_method');
    $this->installConfig(['commerce_order']);

    $currency_importer = $this->container->get('commerce_price.currency_importer');
    $currency_importer->import('JPY');

    OrderItemType::create([
      'id' => 'default',
      'label' => 'Default',
      'orderType' => 'default',
    ])->save();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    // Create an order.
    $this->order = $this->entityTypeManager->getStorage('commerce_order')->create([
      'type' => 'default',
      'order_number' => '1',
      'store_id' => $this->store->id(),
      'state' => 'draft',
    ]);
    $this->order->save();

    $order_item = $this->entityTypeManager->getStorage('commerce_order_item')->create([
      'type' => 'default',
      'unit_price' => [
        'number' => '999',
        'currency_code' => 'JPY',
      ],
    ]);
    $order_item->save();
    $this->order->setItems([$order_item]);
    $this->paymentGateway = PaymentGateway::create(
      [
        'id' => 'gmo_token',
        'plugin' => 'gmo_token',
        'configuration' => [
          'shop_id' => 'shopid',
          'shop_pass' => 'shoppass',
        ]
      ],
    );
    $this->paymentGateway->save();

    $this->order->set('payment_gateway', $this->paymentGateway->id());
    $this->order->save();
  }

  /**
   * Returns the gmo tokengateway.
   *
   * @return \Drupal\commerce_gmo\Plugin\Commerce\PaymentGateway\GmoToken
   *   The gmo gateway.
   *
   * @throws \Exception
   */
  protected function getGmoTokenGateway() {
    $configuration = [
      '_entity' => $this->paymentGateway,
    ] + $this->paymentGateway->getPluginConfiguration();
    $gateway_plugin_manager = $this->container->get('plugin.manager.commerce_payment_gateway');
    return $gateway_plugin_manager->createInstance('gmo_token', $configuration);
  }

  /**
   * Creates HTTP client stub.
   *
   * @param \Psr\Http\Message\ResponseInterface[] $responses
   *   The expected responses.
   *
   * @return \GuzzleHttp\Client
   *   The client.
   */
  protected function createMockHttpClient(array $responses) : Client {
    $mock = new MockHandler($responses);
    $handlerStack = HandlerStack::create($mock);

    return new Client(['handler' => $handlerStack]);
  }

  /**
   * Populates API manager service with mock http client.
   *
   * @param \Psr\Http\Message\ResponseInterface[] $responses
   *   The responses.
   */
  protected function populateHttpClient(array $responses) : void {
    $mock_client = $this->createMockHttpClient($responses);
    $this->container->set('http_client', $mock_client);
    $this->refreshServices();
  }

}
