/**
 * @file
 * Javascript to generate GMO token
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the commerceGmoForm behavior.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.commerceGmoForm = {
    attach: function (context) {
      if (!drupalSettings.commerceGmo || !drupalSettings.commerceGmo.shopId) {
        return;
      }

      $('.gmo-form', context).once('gmo-processed').each(function () {
        const $form = $(this).closest('form');
        const $cardNumber = $('#card-number-element', $form);
        const $expire = $('#expiration-element', $form);
        const $secrityCode = $('#security-code-element', $form);
        const $tokenNumber = $('[name="payment_information[add_payment_method][payment_details][gmo_token]"]', $form);
        const $first6 = $('[name="payment_information[add_payment_method][payment_details][first6]"]', $form);

        $cardNumber.replaceWith('<input type="tel" inputmode="numeric" pattern="[0-9\\s]{13,19}" autocomplete="cc-number" maxlength="19" placeholder="xxxx xxxx xxxx xxxx"  class="field form-text required credit-card-input" required/>');
        $expire.replaceWith('<input type="text" pattern="([0-9]{2}[/]?){2}" placeholder="MM/YY" class="field form-text required expire-input" onkeyup="Drupal.commerceGmo.formatDateString(event);"/>');
        $secrityCode.replaceWith('<input type="number" minlength="3" maxlength="3" size="3" class="field form-text required security-code-input" />');

        // Form submit.
        $form.on('submit.commerce_gmo', function (e) {
          if ($('#gmo-token', $form).val().length > 0) {
            return true;
          }

          const expire = $('.expire-input', $form).val().split('/');

          Multipayment.init(drupalSettings.commerceGmo.shopId);
          Multipayment.getToken({
            cardno: $('.credit-card-input', $form).val(),
            expire: expire[1] + expire[0],
            securitycode: $('.security-code-input', $form).val(),
            tokennumber: $tokenNumber.val()
          }, function(response) {
            // @todo Make this a global function.
            if ( response.resultCode !== '000' ){
              $('#payment-errors').html(Drupal.theme('commerceGmoError', Drupal.t('An error occurred. Please check your payment information.')));
            }
            else {
              // Clean up error messages.
              $form.find('#payment-errors').html('');

              // Set the value of the token field.
              $tokenNumber.val(response.tokenObject.token);
              $first6.val(response.tokenObject.maskedCardNo.substr(0, 6));
            }

            //Submit the form.
            $form.find(':input.button--primary').click();
          });

          // Prevent the form from submitting with the default action.
          if ($cardNumber.length) {
            return false;
          }
        });
      });
    },
  };

  Drupal.commerceGmo =  Drupal.commerceGmo || {};

  /**
   * Formats the credit card expiry date on input.
   *
   * @param event
   */
  Drupal.commerceGmo.formatDateString = function (event) {
    const code = event.keyCode;
    const allowedKeys = [8];
    if (allowedKeys.indexOf(code) !== -1) {
      return;
    }

    event.target.value = event.target.value.replace(
      /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
    ).replace(
      /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
    ).replace(
      /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
    ).replace(
      /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
    ).replace(
      /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
    ).replace(
      /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
    ).replace(
      /\/\//g, '/' // Prevent entering more than 1 `/`
    );
  };

  Drupal.theme.commerceGmoError = function (message) {
    return $('<div class="messages messages--error"></div>').html(message);
  };

})(jQuery, Drupal, drupalSettings);
