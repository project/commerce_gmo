/**
 * @file
 * Javascript for the review panel.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the commerceGmoReview behavior.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.commerceGmoReview = {
    attach: function (context) {
      // Link is not displayed only for the first phase of the process. After that, this does nothing.
      $('.gmo-3ds-init', context).once('gmo-review-processed').each(function () {
        var $form = $(this).closest('form');
        $form.find(":input.button--primary").prop("disabled", true);
        this.click();
      });
    },
  };

})(jQuery, Drupal, drupalSettings);
