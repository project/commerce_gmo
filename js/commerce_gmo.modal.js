/**
 * @file
 * Javascript for the modal form.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the commerceGmoModal behavior.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.commerceGmoModal = {
    attach: function (context) {

      $('.commerce-gmo-acs-form', context).once('gmo-modal-processed').each(function () {
        var $form = $(this).closest('form');
        $form.submit();
      });
    },
  };

})(jQuery, Drupal, drupalSettings);
