<?php

declare(strict_types = 1);

namespace Drupal\commerce_gmo\Form;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the form that submits necessary information for 3DS1.0.
 *
 * The form opens in a modal pop up. None of the fields are visible to the user
 * and the forms is automatically submitted via js provided by
 * commerce_gmo.modal.js. The remote server (specified as action for this form)
 * then redirects back to the current checkout step (specified by the TermUrl
 * input field).
 *
 * @see \Drupal\commerce_gmo\Plugin\Commerce\CheckoutPane\GmoReview::buildPaneForm
 */
class CommerceGmoAcsForm extends FormBase {

  /**
   * Current route.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRoute;

  /**
   * Constructs the form.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route
   *   The current route match.
   */
  public function __construct(CurrentRouteMatch $current_route) {
    $this->currentRoute = $current_route;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'commerce_gmo_acs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, OrderInterface $commerce_order = NULL) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Redirecting ...'),
    ];

    $acs_data = $commerce_order->getData('gmo_acs_data');

    $form['PaReq'] = [
      '#type' => 'hidden',
      '#value' => $acs_data['PaReq'],
    ];

    $form['MD'] = [
      '#type' => 'hidden',
      '#value' => $acs_data['MD'],
    ];

    $terminl_url = Url::fromRoute('commerce_checkout.form')
      ->setRouteParameter('commerce_order', $commerce_order->id())
      ->setRouteParameter('step', 'review')
      ->setAbsolute()
      ->toString();

    $form['TermUrl'] = [
      '#type' => 'hidden',
      '#value' => $terminl_url,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => ['class' => ['invisible']],
    ];
    $form['#attached']['library'][] = 'commerce_gmo/modal';
    $form['#action'] = $acs_data['ACSUrl'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
