<?php

declare(strict_types = 1);

namespace Drupal\commerce_gmo;

/**
 * Data structure needed to execute a transaction.
 */
class ExecTranData {

  /**
   * The access id string.
   *
   * @var string
   */
  protected string $accessId;

  /**
   * The access pass string.
   *
   * @var string
   */
  protected string $accessPass;

  /**
   * The token obtained by the card information tokenization service.
   *
   * @var string
   */
  protected string $token;

  /**
   * The unique order identifier (ie. orderId-orderVersion).
   *
   * @var string
   */
  protected string $orderId;

  /**
   * The return url for 3DS.
   *
   * @var string
   */
  protected string $returnUrl;

  /**
   * Setter magic method.
   *
   * @param string $property
   *   The property name.
   * @param mixed $value
   *   The property value.
   */
  public function __set(string $property, $value): void {
    $this->throwUnlessPropertyExists($property);

    $this->{$property} = $value;
  }

  /**
   * Getter magic method.
   *
   * @param string $property
   *   The property name.
   *
   * @return mixed
   *   The property value.
   */
  public function __get(string $property) {
    $this->throwUnlessPropertyExists($property);

    return $this->{$property};
  }

  /**
   * Checks the existence of a property.
   *
   * @param string $property
   *   The property name.
   *
   * @throws \RuntimeException
   */
  protected function throwUnlessPropertyExists(string $property) {
    if (property_exists($this, $property) !== TRUE) {
      throw new \RuntimeException('Non-existing property ' . $property);
    }
  }

}
