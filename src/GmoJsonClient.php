<?php

declare(strict_types = 1);

namespace Drupal\commerce_gmo;

use Drupal\commerce_price\Price;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;

/**
 * Implements the 3DS2.0 api version calls.
 */
class GmoJsonClient {

  /**
   * Url of the production host.
   */
  const HOST_LIVE = 'https://p01.mul-pay.jp/';

  /**
   * Url of the sandbox host.
   */
  const HOST_SANDBOX = 'https://kt01.mul-pay.jp/';

  /**
   * Gmo credentials needed to communicate with the api.
   *
   * @var string[]
   */
  protected array $credentials = [];

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * The host used for transactions.
   *
   * @var string
   */
  protected string $host;

  /**
   * The http client.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Prevents httpClient to be serialized.
   */
  public function __sleep() {
    $keys = get_object_vars($this);
    unset($keys['httpClient']);
    return array_keys($keys);
  }

  /**
   * Create a Gmo Json client instance.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   * @param string $shop_id
   *   The shop id.
   * @param string $shop_pass
   *   The shop pass.
   * @param bool $sandbox
   *   Whether to use sandbox or live.
   *
   * @return $this
   *   The configured client.
   */
  public static function createClient(Client $http_client, string $shop_id, string $shop_pass, bool $sandbox): GmoJsonClient {
    $client = new static($http_client);
    $client->configure($shop_id, $shop_pass, $sandbox);
    return $client;
  }

  /**
   * Configures the client instance.
   *
   * @param string $shop_id
   *   The shop id.
   * @param string $shop_pass
   *   The shop pass.
   * @param bool $sandbox
   *   Whether to use sandbox or live.
   */
  protected function configure(string $shop_id, string $shop_pass, bool $sandbox): void {
    $this->credentials = [
      'shopID' => $shop_id,
      'shopPass' => $shop_pass,
    ];
    $this->host = ($sandbox) ? static::HOST_SANDBOX : static::HOST_LIVE;
  }

  /**
   * Transaction registration / EntryTran.
   *
   * @param string $order_id
   *   The order id.
   * @param string $job_cd
   *   The job cd: can be CHECK, CAPTURE, AUTH or SAUTH.
   * @param \Drupal\commerce_price\Price $amount
   *   The amount to register.
   * @param string $store_name
   *   The store name to be displayed on the 3DS screen.
   *
   * @return array
   *   Array with response.
   *
   * @throws \Exception
   */
  public function entryTran(string $order_id, string $job_cd, Price $amount, string $store_name) {
    $payload = [
      'tdFlag' => 2,
      'orderID' => $order_id,
      'jobCd' => $job_cd,
      // JPY does not have cents.
      'amount' => (int) $amount->getNumber(),
      'tdTenantName' => $store_name,
      // We require 3DS authentication.
      'tdRequired' => 1,
    ];
    $payload = array_merge($this->credentials, $payload);

    return $this->doCall('payment/EntryTran.json', $payload);
  }

  /**
   * Transaction execution.
   *
   * @param \Drupal\commerce_gmo\ExecTranData $exec_tran_data
   *   The ExecTranData object.
   * @param array $client_fields
   *   Additional fields to be saved upstream.
   * @param int $client_field_flag
   *   0 to not return client field values 1 to return them. Defaults 0.
   *
   * @return array
   *   The result of the transaction execution.
   *
   * @throws \Exception
   */
  public function execTran(ExecTranData $exec_tran_data, array $client_fields = [], int $client_field_flag = 0): array {
    $payload = [
      'accessID' => $exec_tran_data->accessId,
      'accessPass' => $exec_tran_data->accessPass,
      'token' => $exec_tran_data->token,
      'orderID' => $exec_tran_data->orderId,
      // Only Bulk is supported atm.
      'method' => 1,
      'clientFieldFlag' => $client_field_flag,
      'retUrl' => $exec_tran_data->returnUrl,
    ];

    foreach ($client_fields as $key => $client_field) {
      $payload['clientField' . $key] = $client_field;
    }

    return $this->doCall('payment/ExecTran.json', $payload);
  }

  /**
   * Secure transaction execution / 3DS1.0 version.
   *
   * @param string $pa_res
   *   The pa res string.
   * @param string $access_id
   *   The access ID string.
   *
   * @return array
   *   The call results.
   *
   * @throws \Exception
   */
  public function secureTran(string $pa_res, string $access_id): array {
    $payload = [
      'paRes' => $pa_res,
      'md' => $access_id,
    ];

    return $this->doCall('payment/SecureTran.json', $payload);
  }

  /**
   * Secure transaction execution / 3DS2.0 version.
   *
   * @param string $access_id
   *   The access ID string.
   * @param string $access_pass
   *   The access pass string.
   *
   * @return array
   *   The call results.
   *
   * @throws \Exception
   */
  public function secureTran2(string $access_id, string $access_pass): array {
    $payload = [
      'accessID' => $access_id,
      'accessPass' => $access_pass,
    ];

    return $this->doCall('payment/SecureTran2.json', $payload);
  }

  /**
   * Alter a transaction.
   *
   * @param string $access_id
   *   The access ID string.
   * @param string $access_pass
   *   The access pass string.
   * @param string $job_cd
   *   Process classification. Can be one of CAPTURE, AUTH, CANCEL, SALES.
   * @param \Drupal\commerce_price\Price|null $amount
   *   (optional) The amount.
   *
   * @return array
   *   The call results.
   *
   * @throws \Exception
   */
  public function alterTran(string $access_id, string $access_pass, string $job_cd, Price $amount = NULL): array {
    $payload = [
      'accessID' => $access_id,
      'accessPass' => $access_pass,
      'jobCd' => $job_cd,
    ];

    if ($amount) {
      $payload['amount'] = (int) $amount->getNumber();
    }

    if ($job_cd) {
      $payload['jobCd'] = $job_cd;
    }

    $payload = array_merge($this->credentials, $payload);

    return $this->doCall('payment/AlterTran.json', $payload);
  }

  /**
   * Alter a transaction.
   *
   * @param string $access_id
   *   The access ID string.
   * @param string $access_pass
   *   The access pass string.
   * @param string $job_cd
   *   Process classification. Can be one of CAPTURE, AUTH, SAUTH.
   * @param \Drupal\commerce_price\Price|null $amount
   *   (optional) The amount.
   *
   * @return array
   *   The call results.
   *
   * @throws \Exception
   */
  public function changeTran(string $access_id, string $access_pass, string $job_cd, Price $amount = NULL): array {
    $payload = [
      'accessID' => $access_id,
      'accessPass' => $access_pass,
      'jobCd' => $job_cd,
    ];

    if ($amount) {
      $payload['amount'] = (int) $amount->getNumber();
    }

    $payload = array_merge($this->credentials, $payload);

    return $this->doCall('payment/ChangeTran.json', $payload);
  }

  /**
   * Search for a trade.
   *
   * @param string $order_id
   *   The order id as set when creating the transaction.
   *
   * @return array
   *   The call results.
   *
   * @throws \Exception
   */
  public function searchTrade(string $order_id): array {
    $payload = [
      'orderID' => $order_id,
    ];

    $payload = array_merge($this->credentials, $payload);

    return $this->doCall('payment/SearchTrade.json', $payload);
  }

  /**
   * Does the api call.
   *
   * @param string $path
   *   The path without leading slash.
   * @param array $payload
   *   The payload.
   *
   * @return array
   *   The response in array form.
   *
   * @throws \Exception
   */
  protected function doCall(string $path, array $payload) {
    if (empty($this->credentials)) {
      throw new \Exception('Client not configured');
    }

    $options = [
      RequestOptions::JSON => $payload,
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json',
        'charset' => 'UTF-8',
      ],
    ];

    try {
      $result = $this->httpClient->post($this->host . $path, $options);
      return json_decode($result->getBody()->getContents(), TRUE);
    }
    catch (ClientException $e) {
      $content = json_decode($e->getResponse()->getBody()->getContents());
      throw new \Exception("Api returned errCode {$content[0]->errCode}, errInfo {$content[0]->errInfo}");
    }
  }

}
