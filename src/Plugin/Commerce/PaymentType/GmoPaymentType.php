<?php

declare(strict_types = 1);

namespace Drupal\commerce_gmo\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the payment type for GMO.
 *
 * @CommercePaymentType(
 *   id = "gmo_payment",
 *   label = @Translation("GMO payment"),
 *   workflow = "payment_default"
 * )
 */
class GmoPaymentType extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    // Store access id and pass to be able to refund or void transactions.
    $fields['access_id'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Access id'))
      ->setDescription($this->t('The access id obtained with the transaction registration'));
    $fields['access_pass'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Access password'))
      ->setDescription($this->t('The access password obtained with the transaction registration'));
    // @todo Add pmethod type.
    return $fields;
  }

}
