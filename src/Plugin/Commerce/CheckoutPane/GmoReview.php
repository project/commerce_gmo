<?php

declare(strict_types = 1);

namespace Drupal\commerce_gmo\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_gmo\Plugin\Commerce\PaymentGateway\GmoToken;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Adds 3DS handling for GMO payments.
 *
 * This checkout pane is required to handle 3DS authentication. It is not
 * visible to the user it just handles various redirects and GMO calls based
 * on 3 possible states:
 *
 * 1. When the pane is first displayed it registers the transaction with GMO
 *    and executes the first authentication of the payment, fetching the
 *    necessary information for 3ds redirects, that are then executed by js
 *    in commerce_gmo.review.js, that clicks the generated links.
 *    This step is not "visible" to the user, because the described processes
 *    are executed as soon as the pane is rendered.
 * 2. When the user comes back from the 3ds endpoint, this pane authenticates,
 *    stores the obtained information (depending on the 3ds version). At this
 *    point the user sees the last checkout step.
 * 3. When the user submits the last step, this pane executes just stays out of
 *    the way and lets the checkout process continue.
 *
 * @CommerceCheckoutPane(
 *   id = "gmo_review",
 *   label = @Translation("GMO review"),
 *   default_step = "review",
 *   wrapper_element = "container",
 * )
 */
class GmoReview extends CheckoutPaneBase {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentRequest;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->setLogger($container->get('logger.channel.commerce_payment'));
    return $instance;
  }

  /**
   * Sets the logger.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The new logger.
   *
   * @return $this
   *   This object.
   */
  public function setLogger(LoggerInterface $logger): GmoReview {
    $this->logger = $logger;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    $gateway = $this->order->get('payment_gateway');
    if ($gateway->isEmpty() || empty($gateway->entity)) {
      return FALSE;
    }
    return $gateway->entity->getPlugin() instanceof GmoToken;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    try {
      /** @var \Drupal\commerce_gmo\Plugin\Commerce\PaymentGateway\GmoToken $gateway */
      $gateway = $this->order->get('payment_gateway')->entity->getPlugin();
      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $this->order->get('payment_method')->entity;

      $this->assertPaymentMethod($payment_method);

      $query_params = $form_state->getUserInput();
      // If the user is just submitting the last step, do nothing.
      if ((!empty($form_state->getValues()) || !empty($form_state->getUserInput())) && !$this->is3dsRedirect($query_params)) {
        return $pane_form;
      }

      // If the user came from the 3DS check, handle that.
      if ($this->is3dsRedirect($query_params)) {
        $this->handle3dsRedirect($query_params);
        return $pane_form;
      }

      // Otherwise, prepare for the 3DS flow.
      $payment_process_pane = $this->checkoutFlow->getPane('payment_process');
      assert($payment_process_pane instanceof CheckoutPaneInterface);
      $capture = $payment_process_pane->getConfiguration()['capture'];

      $transaction_result = $gateway->registerTransaction($this->order, $capture);

      // This is later stored on the payment entity. These are used for
      // refunding (together with store credentials)
      $this->order->setData('gmo_access_id', $transaction_result['accessID']);
      $this->order->setData('gmo_access_pass', $transaction_result['accessPass']);
      $this->order->save();

      $settlement_result = $gateway->executeTransaction(
        $transaction_result['accessID'],
        $transaction_result['accessPass'],
        $payment_method->getRemoteId(),
        $this->order
      );

      // For BC we support 3DS1.0 as well.
      switch ($settlement_result['acs']) {
        case '0':
          // Cards that do not support 3DS are declined as configured in
          // \Drupal\commerce_gmo\GmoJsonClient::entryTran.
          throw new InvalidRequestException('Workflows without 3DS are not supported.');

        case '1';
          $this->order->setData('gmo_acs_data', [
            // Authentication request message. The value is encoded by the card
            // company.
            'PaReq' => $settlement_result['paReq'],
            // The Transaction ID.
            'MD' => $settlement_result['md'],
            // User authentication password input screen URL provided by the
            // credit card company via GMO.
            'ACSUrl' => $settlement_result['acsurl'],
          ])->save();

          // @see \Drupal\commerce_gmo\Form\CommerceGmoAcsForm
          $pane_form['3ds_link'] = [
            '#type' => 'link',
            '#title' => 'ACS',
            '#attributes' => [
              'class' => ['use-ajax', 'gmo-3ds-init', 'invisible'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => json_encode([
                'title' => $this->t('Authentication'),
                'width' => '30%',
              ]),
            ],
            '#url' => Url::fromRoute('commerce_gmo.acs_modal', ['commerce_order' => $this->order->id()]),
          ];
          break;

        case '2';
          $pane_form['3ds_link'] = [
            '#type' => 'link',
            '#title' => 'ACS',
            '#attributes' => [
              'class' => ['gmo-3ds-init', 'invisible'],
            ],
            // The redirect url provided by the credit card company via GMO.
            '#url' => Url::fromUri($settlement_result['redirectUrl']),
          ];

          break;
      }
    }
    catch (\Exception $e) {
      // Cleanup.
      if (isset($payment_method) && $payment_method instanceof PaymentMethodInterface) {
        $gateway->deletePaymentMethod($payment_method);
      }

      // Cleanup temporary data.
      $gateway->clenupTemporaryOrderData($this->order);

      // Log and redirect to previous step.
      $this->logger->error($e->getMessage());
      $this->messenger()->addError('An error occurred. Please check your payment information.');
      $previous_step = $this->checkoutFlow->getPreviousStepId($this->getStepId());
      $this->checkoutFlow->redirectToStep($previous_step);
    }

    $pane_form['#attached']['library'][] = 'commerce_gmo/review';

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->order);
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($pane_form);
    return $pane_form;
  }

  /**
   * Checks query parameters for 3DS redirect data.
   *
   * @param array $query_params
   *   The array of query parameters.
   *
   * @return bool
   *   True if the parameters contain either 1.0 or 2.0 3DS data.
   */
  protected function is3dsRedirect(array $query_params): bool {
    // Ether the transacion ID (MD) or the access id need to be present, to
    // consider this a 3ds redirect.
    return (isset($query_params['MD']) || isset($query_params['AccessID']));
  }

  /**
   * Handle incoming 3DS redirect.
   *
   * @param array $query_params
   *   The parameters of this query.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function handle3dsRedirect(array $query_params): void {
    if (isset($query_params['MD'], $query_params['PaRes'])) {
      // 3DS1.0.
      if ($query_params['MD'] !== $this->order->getData('gmo_access_id')) {
        throw new InvalidRequestException('3D secure authentication failed');
      }
      $this->order->setData('gmo_pa_res', $query_params['PaRes'])->save();
    }
    elseif (isset($query_params['AccessID'])) {
      // 3DS2.0.
      if ($query_params['AccessID'] !== $this->order->getData('gmo_access_id')) {
        throw new InvalidRequestException('3D secure authentication failed');
      }
    }
    else {
      throw new InvalidRequestException('Missing parameters after 3DS request.');
    }
  }

  /**
   * Asserts that the payment method is neither empty nor expired.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface|null $payment_method
   *   The payment method.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the payment method is empty.
   * @throws \Drupal\commerce_payment\Exception\HardDeclineException
   *   Thrown when the payment method has expired.
   *
   * @see \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase::assertPaymentMethod()
   */
  protected function assertPaymentMethod(PaymentMethodInterface $payment_method = NULL) {
    if (empty($payment_method)) {
      throw new \InvalidArgumentException('The provided payment has no payment method referenced.');
    }
    if ($payment_method->isExpired()) {
      throw new HardDeclineException('The provided payment method has expired');
    }
  }

}
