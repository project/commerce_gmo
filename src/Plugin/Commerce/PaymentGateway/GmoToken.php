<?php

declare(strict_types = 1);

namespace Drupal\commerce_gmo\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_gmo\ExecTranData;
use Drupal\commerce_gmo\GmoJsonClient;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the GMO payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "gmo_token",
 *   label = "GMO Token",
 *   display_label = "GMO Token",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_gmo\PluginForm\Gmo\PaymentMethodAddForm",
 *   },
 *   js_library = "commerce_gmo/token_form",
 *   payment_method_types = {"credit_card"},
 *   payment_type = "gmo_payment"
 * )
 */
class GmoToken extends OnsitePaymentGatewayBase implements SupportsRefundsInterface {

  /**
   * The GMO client.
   *
   * @var \Drupal\commerce_gmo\GmoJsonClient
   */
  protected GmoJsonClient $gmoClient;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $gateway = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $gateway->setGmoClient($container->get('http_client'), $configuration);
    $gateway->setModuleHandler($container->get('module_handler'));
    return $gateway;
  }

  /**
   * Sets the GMO client.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   * @param array $configuration
   *   Plugin configuration.
   */
  public function setGmoClient(Client $http_client, array $configuration) {
    $sandbox = (!isset($configuration['mode']) || $configuration['mode'] == 'test');
    $shop_id = $configuration['shop_id'] ?? '';
    $shop_pass = $configuration['shop_pass'] ?? '';
    $this->gmoClient = GmoJsonClient::createClient($http_client, $shop_id, $shop_pass, $sandbox);
  }

  /**
   * Sets the module handler service.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'shop_id' => '',
      'shop_pass' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['shop_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shop ID'),
      '#default_value' => $this->configuration['shop_id'],
      '#required' => TRUE,
    ];

    $form['shop_pass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shop password'),
      '#default_value' => $this->configuration['shop_pass'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      if (!empty($values['shop_id']) || !empty($values['shop_pass'])) {

        try {
          $client = GmoJsonClient::createClient(\Drupal::httpClient(), $values['shop_id'], $values['shop_pass'], $values['mode'] == 'test');
          $client->searchTrade('1');
        }
        catch (\Exception $e) {
          // If credentials are correct, the error code should be E01110002.
          if (strpos($e->getMessage(), 'E01110002') === FALSE) {
            $form_state->setError($form['shop_id'], $this->t('Invalid credentials'));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['shop_id'] = $values['shop_id'];
      $this->configuration['shop_pass'] = $values['shop_pass'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'gmo_token',
      'number',
      'type',
    ];

    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new InvalidRequestException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $payment_method->card_number = $payment_details['number'];
    $payment_method->card_type = $payment_details['type'];
    $payment_method->setRemoteId($payment_details['gmo_token']);
    // The token expires in 30 min (which can - as per docs - change without
    // notice). We reduce that time by 5s to account for the time it took to do
    // the server request after the JS tokenization.
    $expires = $this->time->getRequestTime() + (60 * 30) - 5;
    $payment_method->setExpiresTime($expires);
    $payment_method->setReusable(FALSE);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // We need to implement this method, but we do not need to do anything
    // with the remote record.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    assert($payment_method instanceof PaymentMethodInterface);
    try {
      $this->assertPaymentMethod($payment_method);
    }
    catch (DeclineException $e) {
      // Delete the payment method if it has expired.
      $this->deletePaymentMethod($payment_method);
      throw new DeclineException($e->getMessage());
    }

    $order = $payment->getOrder();
    assert($order instanceof OrderInterface);

    try {
      $order = $payment->getOrder();
      $access_id = $order->getData('gmo_access_id');
      $payment->set('access_id', $access_id);
      $payment->set('access_pass', $order->getData('gmo_access_pass'));

      $pa_res = $order->getData('gmo_pa_res');
      // If pa_res is set, this means we are dealing with 3DS1.0.
      if ($pa_res) {
        $transaction_result = $this->executeSecureTransaction($pa_res, $order->getData('gmo_access_id'));
        $order->setData('gmo_3ds_version', '1.0')->save();
      }
      else {
        $transaction_result = $this->executeSecureTransaction2($access_id, $order->getData('gmo_access_pass'));
        $order->setData('gmo_3ds_version', '2.0')->save();
      }

      $check_hash_components = [
        $transaction_result['orderID'],
        $transaction_result['forward'],
        $transaction_result['method'],
        $transaction_result['payTimes'],
        $transaction_result['approve'],
        $transaction_result['tranID'],
        $transaction_result['tranDate'],
        $this->getConfiguration()['shop_pass'],
      ];

      if (md5(implode('', $check_hash_components)) !== $transaction_result['checkString']) {
        throw new InvalidRequestException('The checkString did not match');
      }

      // Dates and times are in japanese timezone.
      $transaction_date = DrupalDateTime::createFromTimestamp(strtotime($transaction_result['tranDate']), (new \DateTimeZone('JST')));

      $next_state = $capture ? 'completed' : 'authorization';
      $payment->setRemoteId($transaction_result['tranID']);
      $payment->setAuthorizedTime($transaction_date->format('U'));
      $payment->setState($next_state);
      $payment->save();

      // Remove the temporary data.
      $this->clenupTemporaryOrderData($order);
    }
    catch (\Exception $e) {
      $this->clenupTemporaryOrderData($order);
      $this->deletePaymentMethod($payment_method);
      \Drupal::logger('commerce_gmo')->error($e->getMessage());
      throw new InvalidRequestException();
    }
  }

  /**
   * Cleanup temporary data stored on the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function clenupTemporaryOrderData(OrderInterface $order): void {
    $keys = [
      'gmo_access_id',
      'access_pass',
      'gmo_pa_res',
      'gmo_acs_data',
    ];
    foreach ($keys as $key) {
      if ($order->getData($key)) {
        $order->unsetData($key);
      }
    }
    $order->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    try {
      // We set the new amount, not subtract from the old one.
      $new_amount = $payment->getBalance()->subtract($amount);

      // We cannot change the amount to 0. If it is a full refund, we cancel.
      if ($new_amount->isZero()) {
        $this->gmoClient->alterTran(
          $payment->get('access_id')->value,
          $payment->get('access_pass')->value,
          'CANCEL'
        );
      }
      else {
        $this->gmoClient->changeTran(
          $payment->get('access_id')->value,
          $payment->get('access_pass')->value,
          // When changing the transaction, GMO cancels the previous one and
          // creates a new one.
          'CAPTURE',
          $new_amount
        );
      }
    }
    catch (\Exception $e) {
      throw new InvalidRequestException($e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * Generates an order identifier for the transaction.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return string
   *   The order identifier.
   */
  public function getOrderIdentifier(OrderInterface $order) {
    // Order ids have to be unique per retry. GMO doesn't allow creating
    // new transactions for the same order.
    return $order->id() . '-' . $order->getVersion();
  }

  /**
   * Register a transaction.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   * @param bool $capture
   *   (optional) Whether to capture or authenticate. Defaults to capture.
   *
   * @return array
   *   The result of the call.
   *
   * @throws \Exception
   * @throws \Drupal\commerce_payment\Exception\InvalidRequestException
   */
  public function registerTransaction(OrderInterface $order, bool $capture = TRUE): array {
    $job_cd = $capture ? 'CAPTURE' : 'AUTH';
    $amount = $order->getBalance();
    $store_name = $order->getStore()->getName();

    // Throw exceptions if trying to pay with non JPY currency.
    if ($amount->getCurrencyCode() !== 'JPY') {
      throw new InvalidRequestException('Only paying with JPY is supported');
    }

    return $this->gmoClient->entryTran($this->getOrderIdentifier($order), $job_cd, $amount, $store_name);
  }

  /**
   * Execute the settlement.
   *
   * @param string $access_id
   *   The access id string.
   * @param string $access_pass
   *   The access pass.
   * @param string $token
   *   The token obtained via js.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return array
   *   Call result.
   *
   * @throws \Exception
   */
  public function executeTransaction(string $access_id, string $access_pass, string $token, OrderInterface $order): array {
    $return_url = Url::fromRoute('commerce_checkout.form')
      ->setRouteParameter('commerce_order', $order->id())
      ->setRouteParameter('step', 'review')
      ->setAbsolute()
      ->toString();

    // Review might not be a step. Allow altering that.
    $this->moduleHandler->alter('commerce_gmo_return_url', $return_url);

    $exec_tran_data = new ExecTranData();
    $exec_tran_data->accessId = $access_id;
    $exec_tran_data->accessPass = $access_pass;
    $exec_tran_data->token = $token;
    $exec_tran_data->orderId = $this->getOrderIdentifier($order);
    $exec_tran_data->returnUrl = $return_url;

    return $this->gmoClient->execTran($exec_tran_data);
  }

  /**
   * Execute the 3DS confirmed transaction / 3DS1.0.
   *
   * @param string $pa_res
   *   The pa res string.
   * @param string $access_id
   *   The access id string.
   *
   * @return array
   *   Call result.
   *
   * @throws \Exception
   */
  public function executeSecureTransaction(string $pa_res, string $access_id) {
    return $this->gmoClient->secureTran($pa_res, $access_id);
  }

  /**
   * Execute the 3DS confirmed transaction / 3DS2.0.
   *
   * @param string $access_id
   *   The access id string.
   * @param string $access_pass
   *   The access pass.
   *
   * @return array
   *   Call result.
   *
   * @throws \Exception
   */
  public function executeSecureTransaction2(string $access_id, string $access_pass) {
    return $this->gmoClient->secureTran2($access_id, $access_pass);
  }

}
