<?php

declare(strict_types = 1);

namespace Drupal\commerce_gmo\PluginForm\Gmo;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides payment form for GMO token.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $element['#attributes']['class'][] = 'gmo-form';

    /** @var \Drupal\commerce_gmo\Plugin\Commerce\PaymentGateway\GmoToken $plugin */
    $plugin = $this->plugin;
    $config = $plugin->getConfiguration();

    // Add an element to display errors.
    $element['gmo_errors'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'payment-errors'],
      '#weight' => -10,
    ];

    $element['number'] = [
      '#type' => 'item',
      '#title' => t('Card number'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="card-number-element" class="form-text"></div>',
    ];

    $element['expiration'] = [
      '#type' => 'item',
      '#title' => t('Expiration date'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="expiration-element"></div>',
    ];

    $element['security_code'] = [
      '#type' => 'item',
      '#title' => t('CVC'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="security-code-element"></div>',
    ];

    // Add the js library.
    if ($plugin->getMode() == 'test') {
      $element['#attached']['library'][] = 'commerce_gmo/token_test';
    }
    else {
      $element['#attached']['library'][] = 'commerce_gmo/token_production';
    }
    $element['#attached']['library'][] = 'commerce_gmo/form';

    $element['#attached']['drupalSettings']['commerceGmo'] = [
      'shopId' => $config['shop_id'],
    ];

    // Populated by the JS library.
    $element['first6'] = [
      '#type' => 'hidden',
    ];

    $element['gmo_token'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'gmo-token',
      ],
    ];

    $element['type'] = [
      '#type' => 'hidden',
    ];

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->entity);
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($element);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // Only the token value should be here, so logic for
    // parent::validateCreditCardForm() doesn't apply.
    $values = $form_state->getValue($element['#parents']);

    if (empty($values['gmo_token'])) {
      $form_state->setError($element['number'], $this->t('Could not process your payment. Please retry later or contact support.'));
    }

    // GMO returns less than 6 digits for some cc types.
    $cc_number = substr($values['first6'], 0, 4);

    $card_type = CreditCard::detectType($cc_number);
    if (!$card_type) {
      $form_state->setError($element['number'], $this->t('You have entered a credit card number of an unsupported card type.'));
      return;
    }

    // The credit card number is set to the masked value.
    $form_state->setValueForElement($element['number'], $values['first6']);
    // We set the card type, that is needed in the review pane.
    $form_state->setValueForElement($element['type'], $card_type->getId());
  }

  /**
   * {@inheritdoc}
   */
  protected function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    // Nothing to do here. We unset the values in JS.
  }

}
